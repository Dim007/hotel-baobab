<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Baobab Hotel</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="shortcut icon" href="logo_hotel.png" type="image/png">
	
</head>
<body>
	<main>
		<nav>
			<!-- <div class="log">
			<form action="#" method="post">
				<P>здесь должна быть форма логина</P>
				<form> 
				<input type=value name=login>
				<input type=submit>
			</form>
		</div>
			-->
					<ul>
				<li><a href="MainPage.php" title="Главная страница"><img src="logo_hotel.png" alt="Baobab"></a></li>
				<li><a href="Special_offer_pg.php" title="Спецпредложения" class="anavtext">Спецпредложения</a></li>
				<li><a href="Rooms_pg.php" title="Номера и Цены" class="anavtext">Комнаты</a></li>
				<li><a href="https://baobabsuites.com/en/about-us/careers/" title="Бизнес услуги" class="anavtext">Бизнес услуги</a></li>
				<li><a href="Food_pg.php" title="Рестораны и Бары" class="anavtext">Меню</a></li>
				<li><a href="Contacts_pg.php" title="Контакты" class="anavtext">Контакты</a></li>
			</ul>
		</nav>

<style>
div.example {
  background-color: lightgrey;
  padding: 20px;
}

@media screen and (min-width: 601px) {
  div.example {
    font-size: 80px;
  }
}

@media screen and (max-width: 600px) {
  div.example {
    font-size: 30px;
  }
}
</style>
</head>
<body>

<h2>ДОБРО ПОЖАЛОВАТЬ!!</h2>

<div class="example">Вы успели зарегистроваться</div>

<p><ul>
						<li> Baobab *** находится в центре, в театральном квартале столицы.</li>
						<li> Можно в считанные минуты добраться до все достопримечательностей и музее.</li>
						<li> Наши цены доступны и включают гораздо больше услуг, чем Вы могли бы представить.</li>
						<li> Наш завтрак - богатый шведский стол – накрывается в помещении с прекрасной панорамой города</li>
						<li> Скидки для определенных групп людей.</li>
						<li> Все ванные комнаты оборудованы душевой кабиной, биде, феном, также во всех номерах можно бесплатно пользоваться Сетью Интернет.</li>
					</ul></p>

</body>
</html>
