<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Baobab Hotel</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="shortcut icon" href="logo_hotel.png" type="image/png">
	
</head>
<body>
	<main>
		<nav>
			<!-- <div class="log">
			<form action="#" method="post">
				<P>здесь должна быть форма логина</P>
				<form> 
				<input type=value name=login>
				<input type=submit>
			</form>
		</div>
			-->
			<ul>
				<li><a href="MainPage.php" title="Главная страница"><img src="logo_hotel.png" alt="Baobab"></a></li>
				<li><a href="Special_offer_pg.php" title="Спецпредложения" class="anavtext">Спецпредложения</a></li>
				<li><a href="Rooms_pg.php" title="Номера и Цены" class="anavtext">Комнаты</a></li>
				<li><a href="https://baobabsuites.com/en/about-us/careers/" title="Бизнес услуги" class="anavtext">Бизнес услуги</a></li>
				<li><a href="Food_pg.php" title="Рестораны и Бары" class="anavtext">Меню</a></li>
				<li><a href="Contacts_pg.php" title="Контакты" class="anavtext">Контакты</a></li>
			</ul>
		</nav>
		<section style="padding-top: 5.7%;">
			<h2>Предлагаем вам ознакомиться с меню нашего ресторана:</h2>
			<div>
				<img src="salad.jpg" alt="Салаты" class="menu">
				<img src="coktail.jpg" alt="Закуски" class="menu">
				<img src="dejeuner.jpg" alt="Завтраки" class="menu">
				<img src="poisson.jpg" alt="Горячее рыба" class="menu">
				<img src="viande.jpg" alt="Горячее мясо" class="menu">
				<img src="dessert.jpg" alt="Десерты" class="menu">
			</div>

			
		</section>
		<section>
			
		</section>
		<section>
			
		</section>
		<section>
			
		</section>
		<section>
			
</head>
				
		<section class="section1">
			<div class="fadein" style="width:100%"> 
    			<img src="hotelpic1.jpg"> 
			    <img src="hotelpic2.jpg"> 
			    <img src="hotelpic3.jpg">
			    <img src="hotelpic4.jpg"> 
			</div>
			<div class="insidetextblock" style="left: 10%; top: 20%; ">
				<h2></h2>
		 		<p class="textbrown">"Один из лучших отелей Нижнего Новгорода" <br>
		 		</p>
		 		<p>
		 		__ALL TRAVELLERS ARE WELCOME__
		 		</p>
			</div>
		</section>
		
		<section class="textbrown" style=" padding-top: 56.2%">
			<h2>
				О нашей гостинице:
			</h2>
			<div style="padding-left: 10%; padding-right: 10%; line-height: 1.3;">
				<p>
					<ul>
						<li> Baobab *** находится в центре, в театральном квартале столицы.</li>
						<li> Можно в считанные минуты добраться до все достопримечательностей и музее.</li>
						<li> Наши цены доступны и включают гораздо больше услуг, чем Вы могли бы представить.</li>
						<li> Наш завтрак - богатый шведский стол – накрывается в помещении с прекрасной панорамой города</li>
						<li> Скидки для определенных групп людей.</li>
						<li> Все ванные комнаты оборудованы душевой кабиной, биде, феном, также во всех номерах можно бесплатно пользоваться Сетью Интернет.</li>
					</ul>
				</p>
			</div>
			<h2></h2>
		</section>

	
		</section>
	</main>
	<footer>
		<h2> Tripadvisor reviews: </h2>
		<img src="TA_logo.png" alt="Tripadvisor reviews">
	</footer>
</body>
</html>


