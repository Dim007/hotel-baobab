-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 18 2018 г., 10:56
-- Версия сервера: 5.6.38
-- Версия PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `OurHotel2018`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Booking`
--

CREATE TABLE `Booking` (
  `ID_booking` int(11) NOT NULL,
  `ID_guest` int(11) UNSIGNED NOT NULL,
  `ID_room` int(11) NOT NULL,
  `GetIn` date NOT NULL,
  `GetOut` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Guest`
--

CREATE TABLE `Guest` (
  `ID_guest` int(11) UNSIGNED NOT NULL,
  `User_name` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `numbtel` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Guest`
--

INSERT INTO `Guest` (`ID_guest`, `User_name`, `Password`, `email`, `numbtel`) VALUES
(1, 'USER1', 'user1234', 'user@mail.ru', '+789987'),
(2, '2', 'Users111', 'useruser1', '+897679');

-- --------------------------------------------------------

--
-- Структура таблицы `Rooms`
--

CREATE TABLE `Rooms` (
  `ID_room` int(11) NOT NULL,
  `Rooms` enum('Lux','Polulux','Standart','') NOT NULL,
  `price` int(11) NOT NULL,
  `flats` int(11) NOT NULL,
  `Comments` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Rooms`
--

INSERT INTO `Rooms` (`ID_room`, `Rooms`, `price`, `flats`, `Comments`) VALUES
(1, 'Lux', 449, 2, 'Lux for VIP clients. Include Jacuzzi');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `Booking`
--
ALTER TABLE `Booking`
  ADD PRIMARY KEY (`ID_booking`),
  ADD KEY `ID_guest` (`ID_guest`),
  ADD KEY `ID_room` (`ID_room`);

--
-- Индексы таблицы `Guest`
--
ALTER TABLE `Guest`
  ADD PRIMARY KEY (`ID_guest`);

--
-- Индексы таблицы `Rooms`
--
ALTER TABLE `Rooms`
  ADD PRIMARY KEY (`ID_room`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `Booking`
--
ALTER TABLE `Booking`
  MODIFY `ID_booking` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `Guest`
--
ALTER TABLE `Guest`
  MODIFY `ID_guest` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `Rooms`
--
ALTER TABLE `Rooms`
  MODIFY `ID_room` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `Booking`
--
ALTER TABLE `Booking`
  ADD CONSTRAINT `booking_ibfk_1` FOREIGN KEY (`ID_guest`) REFERENCES `Guest` (`ID_guest`),
  ADD CONSTRAINT `booking_ibfk_2` FOREIGN KEY (`ID_room`) REFERENCES `Rooms` (`ID_room`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
