<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
#more {display: none;}
</style>
</head>
<body>

<h2>Deborah</h2>
<th style="font-size: 30px; width: 33%"><img src="person1.jpg" alt="Deborah" style="width: 100px; float: left;"> Deborah</th>
<p>Отель отличный и не дорогой. Номер хороший, удобный, все есть (зубная паста и щетка, мыло, гель-шампунь для душа, фен, чайник) но нет чая или кофе и нет столовых приборов. Питался (завтрак и ужин шведский стол) в гостинице, все вкусно и разнообразно. <span id="dots">...</span><span id="more">Убирались и меняли полотенце в номере каждый день, но это немного напрягало и приходилось вывешивать табличку "не беспокоить" через день (это мое сугубо личное мнение). Всем рекомендую.</span></p>
<button onclick="myFunction()" id="myBtn">Read more</button>

<script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}
</script>

</body>
</html>
