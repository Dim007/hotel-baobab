<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial;
  font-size: 17px;
}

.container {
  position: relative;
  max-width: 800px;
  margin: 0 auto;
}

.container img {vertical-align: middle;}

.container .content {
  position: absolute;
  bottom: 0;
  background: rgb(0, 0, 0); /* Fallback color */
  background: rgba(0, 0, 0, 0.5); /* Black background with 0.5 opacity */
  color: #f1f1f1;
  width: 100%;
  padding: 20px;
}
</style>
</head>
<body>

<h2>CONTACT US! +7(996)007 0601</h2>

<div class="container">
  <img src="logo_hotel.png" alt="Notebook" style="width:100%;">
  <div class="content">
    <h1>About Us</h1>
    <p>Set on a hill overlooking the sensational southern coast and skyline, our unique Costa Adeje hotel offers 5-star accommodation with a difference, incorporating cool, customised luxury, for a holiday that ticks all the right boxes

Renowned as one of Europe’s premier, all-year-round destinations for luxury family holidays, Costa Adeje is the jewel in a sparkling coastline full of world-class restaurants, shops and facilities guaranteed to put a smile on the face of every family member.

Contact us now for more information on how we can help you create your perfect holiday in Tenerife.
</p>
  </div>
</div>

</body>
</html>
