<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
#more {display: none;}
</style>
</head>
<body>

<h2>Sandra</h2>
<th style="font-size: 30px; width: 33%"><img src="person2.jpg" alt="Sandra" style="width: 100px; float: left;"> Sandra</th>
<p>Останавливаюсь в этой гостинице уже 4 раз (служебные командировки). Уютные, хорошо оформленные номера с бытовой техникой, которая позволяет создать домашний комфорт. Продуманы такие мелочи, как тапочки, бельевые щётки, зубные щетки и паста, гели....Завтраки просто шикарные. Блюда на любой вкус. Я вегетарианка и, тем не менее,<span id="dots">...</span><span id="more"> всегда остаюсь довольна предложенным ассортиментом.Службе такси особое спасибо. Водители встречают на вокзале у вагона. Помогают донести багаж.</td>
</span></p>
<button onclick="myFunction()" id="myBtn">Read more</button>

<script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more"; 
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less"; 
    moreText.style.display = "inline";
  }
}
</script>

</body>
</html>
